import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
    Widget build(BuildContext context) {
      return MaterialApp(
        title: 'Drawer Example',
        theme: ThemeData(
          primarySwatch: Colors.amber,
          accentColor: Colors.purpleAccent
        ),
        home: MyHomePage(),
      );
    }
}

class MyHomePage extends StatelessWidget {
  @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Drawer Example'),
        ), 
        body: Center(child: Text('Drawer Example!'),),
        drawer: Drawer(child:  ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Text('Header'),
              decoration: BoxDecoration(
                color: Colors.amberAccent
              ),
            ),
            ListTile(title: Text('First Item'), onTap: () {
              Navigator.pop(context);
            },)
          ],
        ),
        ) 
      );
    }
}